import { HttpClient } from '@angular/common/http';
import { Injectable, getPlatform } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoriaInterface } from '../interfaces/categoria.interface';
import { LibroInterface } from '../interfaces/libro.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  API_URL_Categorias: string = 'http://167.71.45.117/categorias';
  API_URL_Libros: string = 'http://167.71.45.117/libros';

  constructor(private httpClient: HttpClient) {  }

  getCategorias(): Observable<CategoriaInterface[]> {  
    return this.httpClient.get<CategoriaInterface[]>(this.API_URL_Categorias);
  }

  getLibros(): Observable<LibroInterface[]> {
    return this.httpClient.get<LibroInterface[]>(this.API_URL_Libros);
  }
}
