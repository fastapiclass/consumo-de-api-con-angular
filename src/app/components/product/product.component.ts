import { Component, OnInit } from '@angular/core';
import { CategoriaInterface } from '../../interfaces/categoria.interface';
import { ProductService } from '../../services/product.service';
import { LibroInterface } from '../../interfaces/libro.interface';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit{

    categorias: CategoriaInterface[]=[];
    libros: LibroInterface[]=[];

    constructor(private productService: ProductService) {}

    ngOnInit(): void {
        this.getCategorias()
        this.getLibros()
    }

    getCategorias(){     
      this.productService.getCategorias().subscribe({
        next: (result) => {
          this.categorias = result;
        },
        error: (err) => {
          console.log(err);
        }
      })
    }

    getLibros() {
      this.productService.getLibros().subscribe({
        next: (result) => {
          this.libros = result.map((libro: any) => {
            return {
              ...libro,
              ano: libro['año']
            };
          });
        },
        error: (err) => {
          console.error(err);
        }
      });
    }
}
