export interface LibroInterface {
    codigo: number;
    titulo: string;
    autor: string;
    categoria: string;
    ano: number;
    numeroPaginas: number;
}